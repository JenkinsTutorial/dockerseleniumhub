﻿using Microsoft.VisualStudio.TestTools.UnitTesting;
using OpenQA.Selenium;
using System;
using System.IO;
using System.Reflection;
using System.Threading;

namespace DockerSeleniumHub
{
    public class Actions
    {
        private string _url = "http://168.62.168.191/app";

        public void Login(string username, string password, TestContext TestContext)
        {
            var driverManager = new DriverManager();
            driverManager.Driver.Navigate().GoToUrl(_url);
            driverManager.Driver.FindElement(By.CssSelector("[id = 'username']")).SendKeys(username);
            Thread.Sleep(2 * 1000);
            driverManager.Driver.FindElement(By.CssSelector("[id = 'password']")).SendKeys(password);
            driverManager.Driver.FindElement(By.CssSelector("[id = 'submit']")).Click();
            Thread.Sleep(2 * 1000);
            TakeScreenshot(driverManager, TestContext);
            Thread.Sleep(3 * 1000);
            driverManager.ResetDriver();
        }

        public bool ValidateLogin(string username)
        {
            var driverManager = new DriverManager();
            var userLogon = driverManager.Driver.FindElement(By.XPath("/html/body/div[1]/nav[1]/div[2]/ul[2]/p/strong/a"));
            return userLogon.Text.Contains(username);
        }

        public void TakeScreenshot(DriverManager driverManager, TestContext TestContext)
        {
            try
            {
                string assemblyFolder = Path.GetDirectoryName(Assembly.GetExecutingAssembly().Location) + @"/Screenshots";
                string xmlFileName = Path.Combine(assemblyFolder, "AggregatorItems.xml");
                if (!Directory.Exists(assemblyFolder))
                {
                    Directory.CreateDirectory(assemblyFolder);
                }
                Screenshot ss = ((ITakesScreenshot)driverManager.Driver).GetScreenshot();
                ss.SaveAsFile(assemblyFolder +@"/" + TestContext.TestName + ".jpg", ScreenshotImageFormat.Jpeg);
            }
            catch (Exception e)
            {
                Console.WriteLine(e.Message);
                throw;
            }
        }
    }
}
