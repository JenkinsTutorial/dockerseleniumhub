﻿using OpenQA.Selenium;
using OpenQA.Selenium.Chrome;
using OpenQA.Selenium.Remote;
using System;

namespace DockerSeleniumHub
{
    public class DriverManager
    {
        private  IWebDriver _driver;

        public  IWebDriver Driver
        {
            get
            {
                if (_driver == null)
                {
                    var options = new ChromeOptions();
                    options.AddArguments("-disable-infobars");
                    options.AddArgument("start-maximized");

                    _driver = new RemoteWebDriver(new Uri("http://168.62.168.191:4574/wd/hub"), options.ToCapabilities(), TimeSpan.FromSeconds(300));
                    _driver.Manage().Window.Maximize();
                    //Console.WriteLine("~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~");
                }
                return _driver;
            }
        }

        public  void ResetDriver()
        {
            Driver.Dispose();
            _driver = null;
        }
    }
}
